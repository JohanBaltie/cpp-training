
$HasModule = Get-InstalledModule -Name VSSetup -ErrorAction SilentlyContinue

if(! $HasModule ) {
    Install-Module VSSetup -Scope CurrentUser
}

$VSInstance = Get-VSSetupInstance -All | `
    Select-VSSetupInstance `
        -Product * `
        -Require 'Microsoft.VisualStudio.Component.VC.Tools.x86.x64' `
        -Version '[16.0,17.0]' `
        -Latest

$InstallationPath = $VSInstance.InstallationPath

Import-Module ( Join-Path $InstallationPath "Common7\Tools\Microsoft.VisualStudio.DevShell.dll" )
Enter-VsDevShell `
    -VsInstallPath $InstallationPath `
    -SkipAutomaticLocation `
    -DevCmdArguments "-arch=x64 -host_arch=x64"
