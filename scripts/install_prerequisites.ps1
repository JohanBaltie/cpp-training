# To run this script, you have open a powershell console in administrative mode, go to the script
# directory and invoke:
#
# > Set-ExecutionPolicy Bypass -Scope Process -Force
# > & install_prerequisites.ps1

# Install chocolatey
[System.Net.ServicePointManager]::SecurityProtocol = `
    [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; `
    iex ((New-Object System.Net.WebClient).DownloadString( `
        'https://community.chocolatey.org/install.ps1'))

$env:ChocolateyInstall = Convert-Path "$((Get-Command choco).Path)\..\.."
Import-Module "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"

Write-Output "Preparing locale"

# MSVC should use english
[Environment]::SetEnvironmentVariable('VSLANG', 1033, 'Machine')

# Load codepage
chcp 1252

Update-SessionEnvironment

Write-Output "Installing dependencies"
choco install --force -y --ignorepackagecodes `
    git --params="/WindowsTerminalProfile /NoAutoCrlf /FSMonitor /PseudoConsoleSupport"

# Reverting to normal install
choco install -y --ignorepackagecodes `
    pwsh `
    p4merge `
    python3 `
    nsis `
    jq `
    qtcreator `
    qtcreator-cdbext `
    sublimetext4

# Unpin before forcing install
choco pin remove --name=llvm
choco pin remove --name=conan
choco pin remove --name=cmake

# LLVM Version is important for consistency
choco install --force -y llvm --version 16.0.5

# For now we are using conan v1
choco install --force -y --version 1.60.0 conan

# CMake 3.27 does not work
choco install --force -y --version 3.26.5 cmake

# Freeze LLVM and Conan
choco pin add --name=llvm --version=16.0.5
choco pin add --name=conan --version=1.60.0
choco pin add --name=cmake --version=3.26.5

# Windows hotfix that seems necessary but not automatically downloaded
choco install -y --ignorepackagecodes kb3063858

# Seems important to do those after
# We separate them as they seem prone to failure.
choco install -y --ignorepackagecodes vcredist2013
choco install -y --ignorepackagecodes vcredist2015

choco install -y --ignorepackagecodes windows-sdk-10-version-1903-windbg
choco install -y --ignorepackagecodes visualstudio2019buildtools
choco install -y --ignorepackagecodes visualstudio2019-workload-vctools

Write-Output "Refreshing environment"
Update-SessionEnvironment

Write-Output "Enabling long path option"
$registryPath = "HKLM:\SYSTEM\CurrentControlSet\Control\FileSystem"
$Name = "LongPathsEnabled"
$value = "1"
New-ItemProperty `
    -Path $registryPath -Name $name -Value $value `
    -PropertyType DWORD -Force | Out-Null

Write-Output "Installing Python related tools"
pip install cmake-format
pip install pre-commit

Write-Output "Configuring GIT"

# Backup .gitconfig
cp -Force ${HOME}/.gitconfig ${HOME}/.gitconfig.backup_cpptraining

# Configure
$Name = Read-Host -Prompt "Enter your full name (Firstname/Lastname)"
$Email = Read-Host -Prompt "Enter your email"

git config --global user.name "$Name"
git config --global user.email "$Email"
git config --global merge.tool p4merge
git config --global mergetool.prompt false
git config --global merge.p4merge.cmd  `
    '\"C:/Program Files/Perforce/p4merge.exe\" -le unix \"`$BASE\" \"`$LOCAL\" \"`$REMOTE\" \"`$MERGED\"'

git config --global diff.tool p4merge
git config --global difftool.prompt false
git config --global diff.p4merge.cmd `
    '\"C:/Program Files/Perforce/p4merge.exe\" -le unix \"`$LOCAL\" \"`$REMOTE\"'

git config --global core.eol "lf"
git config --global url.git@gitlab.com:.insteadOf https://gitlab.com/

Write-Output "Configuring conan"
conan remote remove conancenter
conan remote add abbelight http://192.168.118.58:8081/artifactory/api/conan/abbelight --insert 0

Write-Output "Please restart computer"
