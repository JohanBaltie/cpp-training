#include "library_1/library.h"
#include "library_2/library.h"

int main(int, const char *[])
{
    library_1::foo();
    library_2::foo();
    return 0;
}
