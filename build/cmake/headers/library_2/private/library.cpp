#include "library.h"

#include "internal_header.h"
#include <library_1/library.h>

namespace library_2
{
void foo()
{
    bar();
}
void bar() { }
} // namespace library_2
