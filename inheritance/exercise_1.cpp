#include <iostream>

class Shape
{
public:
    Shape()
    {
        std::cout << __PRETTY_FUNCTION__ << ":" << __LINE__ << std::endl;
    }

     ~Shape()
     {
        std::cout << __PRETTY_FUNCTION__ << ":" << __LINE__ << std::endl;
     }

     virtual void applyRatio(double ratio)
     {
        std::cerr << "Cannot apply ratio to Shape" << std::endl;
     }
};

class Circle : public Shape
{
public:
    Circle(double radius)
        : m_radius(radius)
    {
        std::cout << __PRETTY_FUNCTION__ << ":" << __LINE__ << std::endl;
    }

     ~Circle()
     {
        std::cout << __PRETTY_FUNCTION__ << ":" << __LINE__ << std::endl;
     }

     virtual void applyRatio(double ratio)
     {
        std::cout << "Applying" << ratio << " ratio to Circle" << std::endl;
        m_radius *= ratio;
     }

     double getRadius() const
     {
        return m_radius;
     }

 private:
    double m_radius;
};


int main()
{
    std::cout << __PRETTY_FUNCTION__ << ":" << __LINE__ << std::endl;
    {
        Shape shape;

        doubleShape(shape);
    }
    std::cout << __PRETTY_FUNCTION__ << ":" << __LINE__ << std::endl;
    {
        Circle circle(10);

        doubleShape(circle);

        std::cout << "New radius:" << circle.getRadius() << std::endl;
    }
    std::cout << __PRETTY_FUNCTION__ << ":" << __LINE__ << std::endl;
    return 0;
}
