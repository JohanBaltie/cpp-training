#include <functional>
#include <iostream>
#include <memory>
#include <vector>

namespace shape
{

struct Point
{
    using CoordinateProcessor = std::function<void(int, double)>;
    virtual void process(const CoordinateProcessor & processor) = 0;
};

struct Shape
{
    Shape(const std::vector<std::shared_ptr<Point>> & points)
        : m_points(points)
    {
    }

    using PointProcessor = std::function<void(int, Point &)>;
    void process(PointProcessor && processor)
    {
        int index = 0;
        for (auto & point : m_points)
        {
            processor(index++, *point);
        }
    }

private:
    std::vector<std::shared_ptr<Point>> m_points;
};

} // namespace shape

namespace reader
{
auto readFrom(std::istream & stream)
{
    std::vector<std::shared_ptr<shape::Shape>> result;
    bool shouldContinue = true;

    while (shouldContinue)
    {
        // Read some 2D Triangle
        for (int index = 0; index < 3; ++index)
        {
            int x, y;
            std::cout << "Enter a 2D point:";
            stream >> x;
            stream >> y;

            // Create shared_ptr 2D Point and add it to a vector
        }

        // Add the vector to the

        stream >> shouldContinue;
    }

    return result;
}
} // namespace reader

int main()
{
    for(auto & shape : reader::readFrom(std::cin))
    {
        // Print
    }

    return 0;
}
