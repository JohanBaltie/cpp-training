#include <iostream>
#include <vector>

struct Squared
{
    Squared() { std::cout << __PRETTY_FUNCTION__ << std::endl; }

    Squared(int value)
        : value(value)
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
    }

    Squared(const Squared & squared)
        : value(squared.value)
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
    }

    ~Squared() { std::cout << __PRETTY_FUNCTION__ << std::endl; }

    int getSquared() const { return value * value; }

    int value = 0;
};

int main(const int, char **)
{
    const std::vector<Squared> vector{ 1, 2, 3, 4, 5 };

    /* Add code here */

    return 0;
}
