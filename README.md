# sandbox

This repository is for understanding `git` and the interaction with others while using it.

Don't hesitate to express what's bothering you !

## Git

### Exercise 1: Local index and working tree.

1. In this directory. Create a directory with your name like `JohanBaltié`.
2. Add this directory to the staging using `git add`.
3. Check the state of the repository using `git status`.
4. Add a file in this directory, named `README.md`, check the state of the repository.
5. Add this file to the staging, check the state of the repository.
6. Commit using `git commit`.
7. Add some content to this file.
8. Check the state of the repository using `git status`.
9. Add the content of this file to the staging using `git add -p`.
10. Check the state of the repository.
11. Modify the content of the file.
12. Check the state of the repository.
13. Add and commit.

### Exercise 2: Branches

1. Check the log with the command `git log --oneline --graph`.
2. Create your branch with your name `git branch johan_baltie/sandbox_test`.
3. Check the log .
4. Add one commit, Check the log.
5. Change branch using `git checkout johan_baltie/sandbox_test`. Check the log.
6. Add one commit.
7. `git merge main`

### Exercise 3: Working with others

1. Find the SHA-1 of the commit where you've added some content to the file.
2. `git reset --hard origin/main`.
3. Check the log tree.
4. Cherry pick the commit `git cherry-pick XXXX` where XXX is the commit SHA-1.
5. `git push origin HEAD`
6. Wait for others to do the same.
7. `git pull`

## Building

### Exercise 1: Single file

1. Create a build directory.
2. Build `build/one_file/hello_world.cpp`.
3. Launch executable.

### Exercise 2: Two files

1. Create a build directory.
2. Build files in `build/two_files/`.
3. Launch executable.

### Exercise 3: Using header file

1. Edit `build/two_files/main.cpp`.
2. Move the function declaration to another file in a new `include` directory.
3. Include the file.
4. Build.

#### Questions

1. What's happening ?
2. Where is the header file compiled ?
3. What are the potential issues ?
4. Where does the other includes come from ?

### Exercise 4: Basic build using cmake

1. Create build environment using `cmake -S source_directory\one_file -B build_directory`.
2. Build using `cmake --build build_directory`.
3. Create debug build environment for ninja, adding `-DCMAKE_GENERATOR:STRING=Ninja -DCMAKE_BUILD_TYPE:STRING=Debug` to the cmake command line and changing directory.
4. Go to the new build directory, and launch `ninja`.
5. Change the CMake configuration to have the same program as exercice 3 built.

### Exercise 4: Build with libraries

1. Create build environment for ninja with the `library` directory.
2. Build using ninja.

### Exercise 5: Build with headers

1. Create build environment for ninja with the `headers` directory.
2. Build using ninja.

## Qt Creator

### Prerequisite

- Add the toolkit
- Define the environment variable ABL_GITLAB_TOKEN

### Exercise 1: Build

Build the project in exercise 5 with Qt Creator in Debug and in a directory outside the sources.

### Exercise 2: Debug

Put a break point in `main` and launch the debugger.

## Vector

### Exercise 1

Whenever possible, compile and check the result.

1. Open `containers/vector/exercise_1.cpp`
2. Use a `for(const ... : vector)` loop to print all the element with `std::cout << element`.
3. Before printing. Add an element at the end of the vector using `push_back()` method.
4. Assign the vector to a `std::vector<double>` and print this new vector.
5. Assign using `const auto& reference = vector;` , after modify all elements of vector and
   also print `reference`.
6. Read 10 values using `std::cin >> value` and add them to the vector.
7. Print the sum of the values computed using `std::accumulate`
   (see https://en.cppreference.com/w/cpp/algorithm/accumulate)
8. Find the biggest value using `std::max_element`.

### Exercise 2

1. Open `containers/vector/exercise_2.cpp`
2. Print all the element squared using `getSquare`.
3. Replace one element using assignment.
4. Read 10 values using `std::cin >> value` and add them to the vector.
5. Print the sum of the values computed using `std::accumulate`
   (see https://en.cppreference.com/w/cpp/algorithm/accumulate)
6. Find the smallest and biggest value using `std::minmax_element`.
7. Add `std::ostream& operator<<(std::ostream& stream, const Square& square)` operator to print a square like `4^2=16`. Print all the vector.
8. Add prefix increment member operator `Square& operator++()` and postfix increment operator `Square operator++(int)`. Increment and print.
9. Add member operator`Square& operator+=(const Square & rhs)`.
10. Add function operator `Square operator+(Square lhs, const Square & rhs)`.

## Inheritance

### Exercise 1

1. Open `inheritance/exercise_1.cpp`.
2. Create a `doubleShape` function.
3. Make `applyRatio` abstract.
4. Create a `Rectangle` class with `m_width` and `m_height` members.
5. Create a `Square` class that is a specialization of `Rectangle`.
6. Add a `std::string toString() const` virtual abstract method to `Shape` and implement if.
7. Create a `printAll` method taking a `std::vector` of Shape.
8. To implement the `std::vector` of shape try:
   - `std::unique_ptr`
   - `std::shared_ptr`

### Exercice 2

1. Implement what's missing in `inheritance/exercice_2.cpp`.
2. Add a 3D point and a way to choose 2D/or 3D shape
3. Add a slide or scale transformation choice, and apply it before printing.
